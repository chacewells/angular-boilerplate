angular.module('angularDataApp', [])

.controller("DataDisplay", function ($scope, $http, $window, $filter, $log) {
    var orderBy = $filter('orderBy');
    $http.get('data/transactions.json').then(function (response) {
        $scope.transactions = response.data;
    }, function (err) {
        $window.alert("I couldn't get the data: " + err);
    });
    $scope.sort = {
        field: "howmuch",
        reverse: false
    };
    $scope.updateSort = function (field) {
        $scope.sort.reverse = ( $scope.sort.field === field ) ? !$scope.sort.reverse : false;
        $scope.sort.field = field;
        $scope.transactions = orderBy($scope.transactions, field, $scope.sort.reverse);
    };
})
