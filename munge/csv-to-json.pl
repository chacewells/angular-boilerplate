use v5.14;
use Parse::CSV;
use JSON;

my @output;
my $lines = Parse::CSV->new( file => 'transactions.csv', names => 1 );

while (my $line = $lines->fetch) {
    s/^\s+|\s+$//g =~ $line->{$_} for keys %$line;
    push @output, {
        year => int($line->{year}),
        month => int($line->{month}),
        day => int($line->{day}),
        where => $line->{where},
        howmuch => 0+$line->{howmuch},
        how => $line->{how}
    };
}

open my $out, '>', 'transactions.json';
print $out JSON->new->encode(\@output);
